import pandas as pd

def check_metric(target_value: float,
                 real_value: float):
    try:
        assert real_value >= target_value
    except AssertionError:
        return real_value/target_value * 100
    
def test_ds_is_labeled(training_data: pd.DataFrame, label_name: str)->bool:
    '''Проверить, имеется ли в тренировочном наборе данных training_data поле 
    с метками, т.е. столбец с именем label_name'''
    if label_name in training_data.columns:
        return True
    else:
        return False
