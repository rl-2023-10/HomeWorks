# local package
torch==2.1.1
numpy==1.24.4
pandas==2.0.3
scikit-learn==1.3.2
torchvision==0.16.1
matplotlib==3.7.4
pyyaml==6.0.1
python-dotenv==1.0.0

# external requirements
click
Sphinx
coverage
awscli
flake8
python-dotenv>=0.5.1
